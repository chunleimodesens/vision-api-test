import json

import requests

url = "https://test.modesens.com/api/2.0/us/en/product/detail/ssr2/"

payload='purl=versace-stone-washed-crop-denim-jacket-multicolor-75338172&fromweb=1&params='
headers = {
  'authority': 'test2.modesens.com',
  'accept': 'application/json, text/plain, */*',
  'accept-language': 'zh-CN,zh;q=0.9',
  'authorization': 'Bearer MNRvlqL7zx2PYJkl10gk63rE4OpyZK',
  'content-type': 'application/x-www-form-urlencoded',
  'cookie': '_ga=GA1.1.1359197269.1704855015; _fbp=fb.1.1704855015585.656518081; Hm_lvt_5d6195861bd1dc57fe4981c6ed078dd4=1704855058; Hm_lpvt_5d6195861bd1dc57fe4981c6ed078dd4=1704855112; nuxt-6171-experiment=b; category=; i18n_country=us; i18n_locale=en; gcid=1359197269.1704855015; accesscode=ModeSensTest; murls=""; sessionid=r8vv092o1jybhfracxayqoy7huyyzigu; bag_session_id=r8vv092o1jybhfracxayqoy7huyyzigu; login_success=1; refinfo=similarprd62565770; refdate=240110; lsuid=1579985; otoken=MNRvlqL7zx2PYJkl10gk63rE4OpyZK; _clck=1uooqea%7C2%7Cfia%7C0%7C1470; assistedcheckout=1; gsid=1704866804; _uetsid=b1ba2f80af7011ee8462adfa122c7cec; _uetvid=b1ba53b0af7011eeac7ff10a894ff49d; banner_index_1=1; banner_index_2=1; banner_index_3=1; banner_index_4=1; _clsk=oled6z%7C1704866818593%7C1%7C1%7Ct.clarity.ms%2Fcollect; gender=f; prevpageCategory=whymodesens; _ga_DJWKGXM3TP=GS1.1.1704866804.2.1.1704867080.54.0.0; sessionid=hiv1vufg8y5jnz04w1nz027q24i9eaeo; login_success=1; i18n_locale=en; refinfo=u1_nlENLogincodeLogincode; refdate=240105; i18n_country=us; gender=f; murls=""; otoken=MNRvlqL7zx2PYJkl10gk63rE4OpyZK; lsuid=1579985; assistedcheckout=1',
  'origin': 'https://test2.modesens.com',
  'referer': 'https://test2.modesens.com/',
  'refererori': 'https://test2.modesens.com/shop/?txt=https%3A%2F%2Fwww.shopbop.com%2Ffantino-cashmere-collared-cardigan-reformation%2Fvp%2Fv%3D1%2F1548498287.htm%3Fextid%3Daffprg_linkshare_SB-z1KL9yrNyf4%26cvosrc%3Daffiliate.linkshare.z1KL9yrNyf4%26affuid%3D0-usdfO1A6d1d-3zIig-similarprd62565770-1Tz3Ib.1rNOfX.1rNOfW%26sharedid%3D42352%26subid1%3Dz1KL9yrNyf4-uG01r392AVuIRHNzlo.E8g&issearchurl=true',
  'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-origin',
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
