import os

import time
import requests
import random

from csv import DictReader
from google.cloud import vision
from google.cloud import storage
import pandas as pd

#proxy = 'http://127.0.0.1:7892'
#for k in ['http_proxy', 'https_proxy']:
#    os.environ[k] = proxy

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/ec2-user/vision-api/modesens-7ce2847664ce.json'

def list_product_sets(project_id, location):
    """List all product sets.
    Args:
        project_id: Id of the project.
        location: A compute region name.
    """
    client = vision.ProductSearchClient()

    # A resource that represents Google Cloud Platform location.
    location_path = f"projects/{project_id}/locations/{location}"

    print(location_path)
    # List all the product sets available in the region.
    product_sets = client.list_product_sets(parent=location_path)

    # Display the product set information.
    for product_set in product_sets:
        print(f"Product set name: {product_set.name}")
        print("Product set id: {}".format(product_set.name.split("/")[-1]))
        print(f"Product set display name: {product_set.display_name}")
        print("Product set index time: ")
        print(product_set.index_time)


def upload_blob_from_memory(bucket_name, destination_blob_name):
    """Uploads a file to the bucket."""

    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"

    # The contents to upload to the file
    # contents = "these are my contents"

    # The ID of your GCS object
    # destination_blob_name = "storage-object-name"
    url = 'https://assetsprx.matchesfashion.com/img/product/500/1573236_2.jpg'
    r = requests.get(url)

    # with open('./res.jpg', 'wb') as f:
    #     f.write(r.content)

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_string(r.content, content_type='image/jpeg')

    print(
        f"{destination_blob_name} with contents uploaded to {bucket_name}."
    )


def upload_blob_from_csv(bucket_name='vision-api-modesens'):
    destination_blob_path = 'search-images-v1/'
    result = []
    ua_list = ['Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0',
               'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36'
               ]

    with open('./img/ready_to_update_0126_1.csv', mode='r', encoding='utf-8') as f:
        dict_reader = DictReader(f)
        products = list(dict_reader)

    for index, product in enumerate(products):
        print(f'start {index} img: upload {product["id"]}')
        # images = product['images'].split(';')
        for index_1, cover in enumerate([0, 1]):
            print(cover)
            # try:
            #     # headers['authority'] = cover.split('/')[2]
            #     headers['user-agent'] = random.choice(ua_list)
            #     r = requests.get(cover)
            # except Exception as ex:
            #     print(f'======== error for {index}, image index: {index_1} =======')
            #     error_products.add(product["id"])
            #     time.sleep(1)
            #     break
            # if r.status_code != 200 or len(r.content) <= 4903:
            #     print(r.status_code, len(r.content))
            #     error_products.add(product["id"])
            #     continue
            storage_client = storage.Client()
            destination_blob_name = f'{destination_blob_path}{product["id"]}_{index_1}.jpg'
            bucket = storage_client.bucket(bucket_name)
            blob = bucket.blob(destination_blob_name)
            with open(f'./img/0125/{product["id"]}_{index_1}.png', 'rb') as f:
                blob.upload_from_string(f.read(), content_type='image/jpeg')
            temp = {
                'image-url': f'gs://{bucket_name}/{destination_blob_name}',
                'image-id': f'{product["id"]}_{index_1}',
                'product-set-id': 'product_set',
                'product-id': product["id"],
                'product-category': 'apparel-v2',
                'product-display-name': product['name'],
                'labels': f'designer={product["designer"]},merchant={product["merchant"]},gender={product["gender"]},'
                          f'color={product["color"]}',
                'bounding-poly': ''
            }
            result.append(temp)
            time.sleep(1)

    res = pd.DataFrame(result)
    res.to_csv('./img/uploaded_0126_1.csv', index=False)


def localize_objects():
    """Localize objects in the local image.

    Args:
    path: The path to the local file.
    """
    client = vision.ImageAnnotatorClient()

    image = vision.Image()
    image.source.image_uri = 'https://cdn.modesens.cn/umedia/1823982s?w=800'
    objects = client.object_localization(image=image).localized_object_annotations

    print(f"Number of objects found: {len(objects)}")
    for object_ in objects:
        print(f"\n{object_.name} (confidence: {object_.score})")
        print("Normalized bounding polygon vertices: ")
        for vertex in object_.bounding_poly.normalized_vertices:
            print(f" - ({vertex.x}, {vertex.y})")


def detect_crop_hints():
    """Detects crop hints in an image."""
    client = vision.ImageAnnotatorClient()

    # with open(path, "rb") as image_file:
    #     content = image_file.read()
    # image = vision.Image(content=content)

    image = vision.Image()
    image.source.image_uri = 'https://cdn.modesens.cn/umedia/1823982s?w=800'

    crop_hints_params = vision.CropHintsParams(aspect_ratios=[1.77])
    image_context = vision.ImageContext(crop_hints_params=crop_hints_params)

    response = client.crop_hints(image=image, image_context=image_context)
    hints = response.crop_hints_annotation.crop_hints

    for n, hint in enumerate(hints):
        print(f"\nCrop Hint: {n}")

        vertices = [
            f"({vertex.x},{vertex.y})" for vertex in hint.bounding_poly.vertices
        ]

        print("bounds: {}".format(",".join(vertices)))

    if response.error.message:
        raise Exception(
            "{}\nFor more info on error messages, check: "
            "https://cloud.google.com/apis/design/errors".format(response.error.message)
        )


if __name__ == '__main__':
    # print("sss")
    # _project_id = 'vision-api-410907'
    # # # _location = 'us-west1'
    # locations = ['us-west1', 'us-east1', 'asia-east1']
    # for _location in locations:
    #     list_product_sets(_project_id, _location)

    upload_blob_from_memory('product-images', '1573236_2.jpg')
    # upload_blob_from_csv()
    # localize_objects()
    # detect_crop_hints()
    # upload_blob_from_csv()
